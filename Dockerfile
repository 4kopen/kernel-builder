FROM ubuntu:xenial

# Install dependencies
RUN apt-get update               \
 && apt-get -y -q upgrade        \
 && apt-get -y -q install        \
    bc                           \
    binutils-arm-linux-gnueabihf \
    build-essential              \
    ccache                       \
    gcc-arm-linux-gnueabihf      \
    gccgo-4.7-arm-linux-gnueabi  \
    gcc-aarch64-linux-gnu        \
    git                          \
    libncurses-dev               \
    libssl-dev                   \
    u-boot-tools                 \
    wget                         \
    xz-utils                     \
 && apt-get clean


# Install DTC
RUN wget http://ftp.fr.debian.org/debian/pool/main/d/device-tree-compiler/device-tree-compiler_1.4.0+dfsg-1_amd64.deb -O /tmp/dtc.deb \
 && dpkg -i /tmp/dtc.deb \
 && rm -f /tmp/dtc.deb

RUN apt-get -y -q install \
	bison \
	flex \
	&& apt-get clean
	
# Fetch the kernel
ENV KVER=stable              \
    CCACHE_DIR=/ccache       \
    SRC_DIR=/usr/src         \
    DIST_DIR=/dist           \
    LINUX_DIR=/usr/src/linux \
    LINUX_REPO_URL=https://bitbucket.org/4kopen/linux.git \
	BRANCH=4kopen-bringup
	
RUN mkdir -p ${SRC_DIR} ${CCACHE_DIR} ${DIST_DIR}  \
 && cd /usr/src                                    \
 && git clone -b ${BRANCH} ${LINUX_REPO_URL}       \
 && ln -s ${SRC_DIR}/linux-${KVER} ${LINUX_DIR}
WORKDIR ${LINUX_DIR}


# Update git tree
RUN git fetch --tags

# Area to export stuff
RUN mkdir external
VOLUME /usr/src/linux/external

CMD (git pull; \
	cp config.pete .config; \
	make all uImage ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- CC=arm-linux-gnueabi-gcc-4.7 LOADADDR=0x82000000 -j6 \
	make modules_install INSTALL_MOD_PATH=/usr/src/linux/external ARCH=arm CROSS_COMPILE=arm-linux-gnueabi- CC=arm-linux-gnueabi-gcc-4.7 \
	cp arch/arm/boot/uImage arch/arm/boot/dts/stih418-b2264.dtb external)
